from django.urls import path
from feedback.views import FeedbackView
app_name = "feedback"

urlpatterns = [
    path(f'{app_name}/<int:pk>', FeedbackView.as_view()),
]