import json

from django.forms import model_to_dict
from rest_framework.response import Response
from rest_framework.views import APIView
from feedback.models import Feedback

class FeedbackView(APIView):
    def get(self, request, *args, **kwargs):
        feedback_id = kwargs.get('pk')
        if feedback_id:
            feedback = Feedback.objects.filter(id=feedback_id).get()
        else:
            feedback = Feedback.objects.last()

        return Response(model_to_dict(feedback))

    def post(self, request):
        email = request.data.get('email')
        text = request.data.get('text')
        feedback = Feedback.objects.create(email=email, text=text)
        return Response(json.dumps(feedback))