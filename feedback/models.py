from django.db import models

class Feedback(models.Model):
    email = models.EmailField()
    text = models.TextField()

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'

    def __str__(self):
        return f'{self.email} - {self.text}'
